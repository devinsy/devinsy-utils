/*
 * Copyright (C) 2013,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
import java.io.File;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.PatternLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.FileIterator;

/**
 * The Class FileIteratorSandbox.
 */
public class FileIteratorSandbox
{
	private static Logger logger;

	static
	{
		// Initialize logger.
		org.apache.log4j.BasicConfigurator.configure();
		org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.DEBUG);

		logger = LoggerFactory.getLogger(FileIteratorSandbox.class);

		//
		org.apache.log4j.Logger defaultLogger = org.apache.log4j.Logger.getRootLogger();
		defaultLogger.removeAllAppenders();
		defaultLogger.addAppender(new ConsoleAppender(new PatternLayout("%d{ISO8601} - dutils [%-5p] %34.34c.%-25M - %m%n")));

		//
		logger.debug("Log initialized.");
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(final String[] args)
	{
		test();
	}

	/**
	 * Test.
	 */
	protected static void test()
	{
		System.out.println("user.dir=" + System.getProperty("user.dir"));

		try
		{
			// File f = new File("TestTree/DirectoryOne/titi2");
			// File f = new File("/home/cpm/.kde//cache-cpmstar");
			File f = new File("tests/TestTree/xine.jpg");
			System.out.println("exists=" + f.exists());
			System.out.println("canonical path = " + f.getCanonicalPath());
			System.out.println("absolute  path = " + f.getAbsolutePath());
			System.out.println("name           = " + f.getName());
			System.out.println("parent         = " + f.getParent());
			System.out.println("path           = " + f.getPath());
			System.out.println("path           = " + f.lastModified());
			System.out.println("path           = " + f.length());
			System.out.println("path           = " + f.isFile());
			System.out.println("path           = " + f.isDirectory());
			System.out.println("list           = " + f.list());

			System.out.println("----");
			// FileIterator i = new FileIterator(new File("tests/TestTree"));
			FileIterator i = new FileIterator(new File("tests/TestTree/xine.jpg"), null, true);
			// FileIterator i = new FileIterator(new
			// File("/home/cpm/.kde/cache-cpmstar"), ".*cache.*", false);
			// FileIterator i = new FileIterator(new File("tests/TestTree"),
			// ".*dsc.*", false);
			// FileIterator i = new FileIterator(new
			// File("/home/cpm/Images/Photos/"));
			// FileIterator i = new FileIterator(new
			// File("/home/cpm/Images/Photos/"), ".*\\.(JPG|jpg)", false);
			// FileIterator i = new FileIterator(new
			// File("/home/cpm/Images/Photos/"), ".*anni_moi.*", false);

			while (i.hasNext())
			{
				// System.out.println(i.toString());
				System.out.println("File=[" + i.next().getPath() + "]");
			}
			i.reset();
			System.out.println("Cardinal=" + i.finalCountdown());
		}
		catch (Exception exception)
		{
			System.out.println("ERROR:" + exception.getMessage());
		}
	}
}
