/*
 * Copyright (C) 2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.devinsy.util.strings.StringList;

/**
 * The Class FileToolsTest.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */

public class FileToolsTest
{
	public final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FileToolsTest.class);

	/**
	 * Before.
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.ERROR);
	}

	/**
	 * Load to string list URL 01.
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void loadToStringListURL01() throws IOException
	{
		//
		this.logger.debug("===== test starting...");
		//
		StringList source = FileTools.loadToStringList(FileTools.class.getResource("/fr/devinsy/util/lines.txt"));

		//
		Assert.assertEquals(4, source.size());
		Assert.assertEquals("trois", source.get(3 - 1));

		//
		this.logger.debug("===== test done.");
	}

	/**
	 * Test get extension.
	 */
	@Test
	public void testGetExtension()
	{
		//
		this.logger.debug("===== test starting...");

		//
		String extension = FileTools.getExtension("test.ext");

		//
		Assert.assertEquals(extension, "ext");

		//
		this.logger.debug("===== test done.");
	}
}
