/*
 * Copyright (C) 2009-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

/**
 * This class defines a simple average manager. For example, it is useful for
 * millisecond. The maximum value available in input is one day in millisecond.
 */
public class SimpleAveragemeter
{
	private long sum;
	private long cardinal;
	private long MAX_ADD = 1 * 24 * 60 * 60 * 1000; // One day in millisecond.

	/**
	 * Instantiates a new simple averagemeter.
	 */
	public SimpleAveragemeter()
	{
		this.reset();
	}

	/**
	 * Adds the.
	 * 
	 * @param value
	 *            the value
	 */
	synchronized public void add(final long value)
	{
		// Manage the sum limit.
		if ((this.sum > Long.MAX_VALUE / 2) && (this.cardinal % 2 == 0))
		{
			this.sum = this.sum / 2;
			this.cardinal = this.cardinal / 2;
		}

		// Add the new value.
		if (this.sum > this.MAX_ADD)
		{
			this.sum += this.MAX_ADD;
			this.cardinal += 1;
		}
		else
		{
			this.sum += value;
			this.cardinal += 1;
		}
	}

	/**
	 * Average.
	 * 
	 * @return the long
	 */
	synchronized public long average()
	{
		long result;

		if (this.cardinal == 0)
		{
			result = 0;
		}
		else
		{
			result = this.sum / this.cardinal;
		}

		//
		return result;
	}

	/**
	 * Cardinal.
	 * 
	 * @return the long
	 */
	public long cardinal()
	{
		long result;

		result = this.cardinal;

		//
		return result;
	}

	/**
	 * Reset.
	 */
	synchronized public void reset()
	{
		this.sum = 0;
		this.cardinal = 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = Long.toString(this.average());

		//
		return result;
	}

	/**
	 * Value.
	 * 
	 * @return the long
	 */
	public long value()
	{
		long result;

		result = this.average();

		//
		return result;
	}
}
