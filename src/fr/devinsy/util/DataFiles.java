/*
 * Copyright (C) 2008-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.util.ArrayList;

/**
 * This class is a collection of DataFile objects whit some specific methods.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class DataFiles extends ArrayList<DataFile>
{
	private static final long serialVersionUID = -4584622422555785456L;

	/**
	 * Instantiates a new data files.
	 */
	public DataFiles()
	{
		super();
	}

	/**
	 * Instantiates a new data files.
	 * 
	 * @param source
	 *            the source
	 */
	public DataFiles(final DataFiles source)
	{
		super(source);
	}

	/**
	 * Gets the by content id.
	 * 
	 * @param id
	 *            the id
	 * @return the by content id
	 */
	public DataFiles getByContentId(final int id)
	{
		DataFiles result = new DataFiles();

		for (int nDataFile = 0; nDataFile < this.size(); nDataFile++)
		{
			DataFile contentFile = this.getByIndex(nDataFile);

			if (contentFile.contentId() == id)
			{
				result.add(contentFile);
			}
		}

		//
		return result;
	}

	/**
	 * Gets the by id.
	 * 
	 * @param id
	 *            the id
	 * @return the by id
	 */
	public DataFile getById(final int id)
	{
		DataFile result = null;

		boolean ended = false;
		int nDataFile = 0;
		while (!ended)
		{
			if (nDataFile >= this.size())
			{
				ended = true;
				result = null;
			}
			else
			{
				DataFile contentFile = this.getByIndex(nDataFile);

				if (id == contentFile.id())
				{
					ended = true;
					result = contentFile;
				}
				else
				{
					nDataFile += 1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Gets the by index.
	 * 
	 * @param index
	 *            the index
	 * @return the by index
	 */
	public DataFile getByIndex(final int index)
	{
		DataFile result;

		result = super.get(index);

		//
		return result;
	}

	/**
	 * Gets the by name.
	 * 
	 * @param name
	 *            the name
	 * @return the by name
	 */
	public DataFile getByName(final String name)
	{
		DataFile result = null;

		if ((name == null) || (name.equals("")))
		{
			result = null;
		}
		else
		{
			boolean ended = false;
			int dataFileIndex = 0;
			while (!ended)
			{
				if (dataFileIndex >= this.size())
				{
					ended = true;
					result = null;
				}
				else
				{
					DataFile contentFile = this.getByIndex(dataFileIndex);

					if (name.equals(contentFile.name()))
					{
						ended = true;
						result = contentFile;
					}
					else
					{
						dataFileIndex += 1;
					}
				}
			}
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#toString()
	 */
	@Override
	public String toString()
	{
		StringBuffer result = new StringBuffer();

		for (int nDataFile = 0; nDataFile < this.size(); nDataFile++)
		{
			DataFile contentFile = this.getByIndex(nDataFile);
			result.append("== " + contentFile.name() + "\n");
			result.append("contentFile " + nDataFile + " - " + contentFile.name() + "\n");
		}

		//
		return (result.toString());
	}
}
