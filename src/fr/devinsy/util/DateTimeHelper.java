/*
 * Copyright (C) 2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.util.Calendar;

/**
 * This class groups function to help in Calendar manipulation.
 */
public class DateTimeHelper
{
	// static private final Logger logger =
	// LoggerFactory.getLogger(DateTimeHelper.class);

	private static final String EUROPEAN_DATE_FORMAT = "%02d/%02d/%04d %02d:%02d:%02d";
	private static final String RAW_DATE_FORMAT = "%04d%02d%02d %02d:%02d:%02d";
	private static final String ISO_DATE_FORMAT = "%04d-%02d-%02d %02d:%02d:%02d";
	private static final String AMERICAN_DATE_FORMAT = "%02d/%02d/%04d %02d:%02d:%02d";

	private static final String EUROPEAN_DATE_PATTERN = "^([0123]{0,1}\\d)/([01]{0,1}\\d)/(\\d\\d\\d\\d)$";
	private static final String RAW_DATE_PATTERN = "^(\\d\\d\\d\\d)([01]\\d)([0123]\\d)$";
	private static final String ISO_DATE_PATTERN = "^(\\d\\d\\d\\d)-([01]\\d)-([0123]\\d)$";
	private static final String AMERICAN_DATE_PATTERN = "^([01]{0,1}\\d)/([0123]{0,1}\\d)/(\\d\\d\\d\\d)$";

	/**
	 * American format.
	 * 
	 * @param time
	 *            the time
	 * @return the string
	 */
	public static String americanFormat(final Calendar time)
	{
		String result;

		if (time == null)
		{
			result = "";
		}
		else
		{
			result = String.format(AMERICAN_DATE_FORMAT, time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH), time.get(Calendar.YEAR), time.get(Calendar.HOUR_OF_DAY),
					time.get(Calendar.MINUTE), time.get(Calendar.SECOND));
		}

		//
		return result;
	}

	/**
	 * European format.
	 * 
	 * @param time
	 *            the time
	 * @return the string
	 */
	public static String europeanFormat(final Calendar time)
	{
		String result;

		if (time == null)
		{
			result = "";
		}
		else
		{
			result = String.format(EUROPEAN_DATE_FORMAT, time.get(Calendar.DAY_OF_MONTH), time.get(Calendar.MONTH) + 1, time.get(Calendar.YEAR), time.get(Calendar.HOUR_OF_DAY),
					time.get(Calendar.MINUTE), time.get(Calendar.SECOND));
		}

		//
		return result;
	}

	/**
	 * ISO format.
	 * 
	 * @param time
	 *            the time
	 * @return the string
	 */
	public static String ISOFormat(final Calendar time)
	{
		String result;

		if (time == null)
		{
			result = "";
		}
		else
		{
			result = String.format(ISO_DATE_FORMAT, time.get(Calendar.YEAR), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH), time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE),
					time.get(Calendar.SECOND));
		}

		//
		return result;
	}

	/**
	 * Raw format.
	 * 
	 * @param time
	 *            the time
	 * @return the string
	 */
	public static String rawFormat(final Calendar time)
	{
		String result;

		if (time == null)
		{
			result = "";
		}
		else
		{
			result = String.format(RAW_DATE_FORMAT, time.get(Calendar.YEAR), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH), time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE),
					time.get(Calendar.SECOND));
		}

		//
		return result;
	}
}
