/*
 * Copyright (C) 2008-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.util.Date;

/**
 * The Class SimpleChronometer.
 */
public class SimpleChronometer
{
	//
	private long firstTime;

	/**
	 * Instantiates a new simple chronometer.
	 */
	public SimpleChronometer()
	{
		this.reset();
	}

	/**
	 * Interval.
	 * 
	 * @return the long
	 */
	public long interval()
	{
		long result;

		result = new Date().getTime() - this.firstTime;

		//
		return result;
	}

	/**
	 * Reset.
	 */
	public void reset()
	{
		this.firstTime = new Date().getTime();
	}

	/**
	 * TO BE COMPLETED.
	 * 
	 * @param interval
	 *            the interval
	 * @return the string
	 */
	public static String humanString(final long interval)
	{
		String result;

		if (interval < 1000)
		{
			result = interval + "ms";
		}
		else if (interval < 60 * 1000)
		{
			result = interval / 1000 + "," + interval % 1000 + "s";
		}
		else if (interval < 60 * 60 * 1000)
		{
			result = interval / 1000 + "," + interval % 1000 + "s";
		}
		else if (interval < 24 * 60 * 60 * 1000)
		{
			result = interval / 1000 + "," + interval % 1000 + "s";
		}
		else if (interval < 7 * 24 * 60 * 60 * 1000)
		{
			result = interval / 1000 + "," + interval % 1000 + "s";
		}
		else
		// if (interval < 7*24*60*60*1000)
		{
			result = interval / 1000 + "," + interval % 1000 + "s";
		}

		//
		return result;
	}

	/**
	 * Short human string.
	 * 
	 * @param interval
	 *            the interval
	 * @return the string
	 */
	public static String shortHumanString(final long interval)
	{
		String result;

		if (interval < 1000)
		{
			result = interval + " ms";
		}
		else if (interval < 2 * 1000)
		{
			result = interval / 1000 + " seconde";
		}
		else if (interval < 60 * 1000)
		{
			result = interval / 1000 + " secondes";
		}
		else if (interval < 2 * 60 * 1000L)
		{
			result = interval / (60 * 1000L) + " minute";
		}
		else if (interval < 60 * 60 * 1000L)
		{
			result = interval / (60 * 1000L) + " minutes";
		}
		else if (interval < 2 * 60 * 60 * 1000L)
		{
			result = interval / (60 * 60 * 1000L) + " heure";
		}
		else if (interval < 24 * 60 * 60 * 1000L)
		{
			result = interval / (60 * 60 * 1000L) + " heures";
		}
		else if (interval < 2 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (24 * 60 * 60 * 1000L) + " jour";
		}
		else if (interval < 7 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (24 * 60 * 60 * 1000L) + " jours";
		}
		else if (interval < 2 * 7 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (7 * 24 * 60 * 60 * 1000L) + " semaine";
		}
		else if (interval < 30 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (7 * 24 * 60 * 60 * 1000L) + " semaines";
		}
		else if (interval < 52 * 7 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (30 * 24 * 60 * 60 * 1000L) + " mois";
		}
		else if (interval < 2 * 52 * 7 * 24 * 60 * 60 * 1000L)
		{
			result = interval / (52 * 7 * 24 * 60 * 60 * 1000L) + " année";
		}
		else
		{
			result = interval / (52 * 7 * 24 * 60 * 60 * 1000L) + " années";
		}

		//
		return result;
	}
}
