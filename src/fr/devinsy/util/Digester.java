/*
 * Copyright (C) 2006,2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;

/**
 * This class is a helper to use MessageDigester class.
 * 
 * @deprecated because of DigestUtils from apache-commons-codec is doing the
 *             same but better.
 */
@Deprecated
public class Digester
{

	/**
	 * "SHA-1", "MD5", "SHA-256", and "SHA-512".
	 * 
	 * @param digestMethod
	 *            the digest method
	 * @param file
	 *            the file
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	public static String computeHash(final String digestMethod, final File file) throws Exception
	{
		String result;

		if ((file == null) || (!file.exists()))
		{
			result = null;
		}
		else
		{
			// byte[] hash = null;

			InputStream source = null;
			try
			{
				MessageDigest digester = MessageDigest.getInstance(digestMethod);
				source = new FileInputStream(file);
				boolean ended = false;
				int bytesNumber;
				byte[] buffer = new byte[100 * 1024];
				while (!ended)
				{
					bytesNumber = source.read(buffer);
					if (bytesNumber == -1)
					{
						ended = true;
					}
					else
					{
						digester.update(buffer, 0, bytesNumber);
					}
				}

				byte[] digest = digester.digest();

				result = humanReadableDigest(digest);
			}
			catch (java.security.NoSuchAlgorithmException exception)
			{
				throw new Exception("Digest method unknown.", exception);
			}
			catch (java.io.FileNotFoundException exception)
			{
				throw new Exception("File not found (" + exception.getMessage() + ")", exception);
			}
			catch (java.io.IOException exception)
			{
				throw new Exception("Error reading file.", exception);
			}
			finally
			{
				if (source != null)
				{
					source.close();
				}
			}
		}

		//
		return result;
	}

	/**
	 * Human readable digest.
	 * 
	 * @param digest
	 *            the digest
	 * @return the string
	 */
	public static String humanReadableDigest(final byte[] digest)
	{
		String result;

		StringBuffer hashString = new StringBuffer();

		for (int letterIndex = 0; letterIndex < digest.length; ++letterIndex)
		{
			String hex = Integer.toHexString(digest[letterIndex]);
			if (hex.length() == 1)
			{
				hashString.append('0');
				hashString.append(hex.charAt(hex.length() - 1));
			}
			else
			{
				hashString.append(hex.substring(hex.length() - 2));
			}
		}

		result = hashString.toString();

		//
		return result;
	}
}
