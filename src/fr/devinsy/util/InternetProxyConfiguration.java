/*
 * Copyright (C) 2009-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

/**
 * The Class InternetProxyConfiguration.
 */
public class InternetProxyConfiguration
{
	private String host;
	private int port;
	private String login;
	private String password;

	/**
	 * Instantiates a new internet proxy configuration.
	 */
	public InternetProxyConfiguration()
	{
		this.host = "";
		this.port = 0;
		this.login = "";
		this.password = "";
	}

	/**
	 * Instantiates a new internet proxy configuration.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @param login
	 *            the login
	 * @param password
	 *            the password
	 */
	public InternetProxyConfiguration(final String host, final int port, final String login, final String password)
	{
		//
		if (host == null)
		{
			this.host = "";
		}
		else
		{
			this.host = host;
		}

		//
		this.port = port;

		//
		if (login == null)
		{
			this.login = "";
		}
		else
		{
			this.login = login;
		}

		//
		if (password == null)
		{
			this.password = "";
		}
		else
		{
			this.password = password;
		}
	}

	/**
	 * Instantiates a new internet proxy configuration.
	 * 
	 * @param host
	 *            the host
	 * @param port
	 *            the port
	 * @param login
	 *            the login
	 * @param password
	 *            the password
	 * @throws Exception
	 *             the exception
	 */
	public InternetProxyConfiguration(final String host, final String port, final String login, final String password) throws Exception
	{
		//
		if (host == null)
		{
			this.host = "";
		}
		else
		{
			this.host = host;
		}

		//
		if ((port == null) || (port.trim().length() == 0))
		{
			this.port = 0;
		}
		else
		{
			try
			{
				this.port = Integer.parseInt(port);
			}
			catch (Exception exception)
			{
				String errorMessage = "Incorrect PROXY port value.";
				throw new Exception(errorMessage, exception);
			}
		}

		//
		if (login == null)
		{
			this.login = "";
		}
		else
		{
			this.login = login;
		}

		//
		if (password == null)
		{
			this.password = "";
		}
		else
		{
			this.password = password;
		}
	}

	/**
	 * Host.
	 * 
	 * @return the string
	 */
	public String host()
	{
		String result;

		result = this.host;

		//
		return result;
	}

	/**
	 * Checks if is initialized.
	 * 
	 * @return true, if is initialized
	 */
	public boolean isInitialized()
	{
		boolean result;

		if ((this.host.length() > 0) && (this.port > 0))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Login.
	 * 
	 * @return the string
	 */
	public String login()
	{
		String result;

		result = this.login;

		//
		return result;
	}

	/**
	 * Password.
	 * 
	 * @return the string
	 */
	public String password()
	{
		String result;

		result = this.password;

		//
		return result;
	}

	/**
	 * Port.
	 * 
	 * @return the int
	 */
	public int port()
	{
		int result;

		result = this.port;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		String login;
		if (this.login.length() == 0)
		{
			login = "";
		}
		else
		{
			login = "********";
		}

		String password;
		if (this.password.length() == 0)
		{
			password = "";
		}
		else
		{
			password = "********";
		}

		result = "(" + this.host + "," + this.port + "," + login + "," + password + ")";

		//
		return result;
	}
}
