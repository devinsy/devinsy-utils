/*
 * Copyright (C) 2009-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

/**
 * Useful for display beautiful percentage value as string.
 * 
 * @author cpm
 */
public class Fraction
{
	private long numerator;
	private long denominator;

	/**
	 * Instantiates a new fraction.
	 * 
	 * @param numerator
	 *            the numerator
	 * @param denominator
	 *            the denominator
	 */
	public Fraction(final long numerator, final long denominator)
	{
		this.numerator = numerator;
		this.denominator = denominator;
	}

	/**
	 * Denominator.
	 * 
	 * @return the long
	 */
	public long denominator()
	{
		long result;

		result = this.denominator;

		//
		return result;
	}

	/**
	 * Numerator.
	 * 
	 * @return the long
	 */
	public long numerator()
	{
		long result;

		result = this.numerator;

		//
		return result;
	}

	/**
	 * Percentage.
	 * 
	 * @return the long
	 * @throws Exception
	 *             the exception
	 */
	public long percentage() throws Exception
	{
		long result;

		result = percentage(this.numerator, this.denominator);

		//
		return result;
	}

	/**
	 * Percentage full string.
	 * 
	 * @return the string
	 */
	public String percentageFullString()
	{
		String result;

		result = percentageFullString(this.numerator, this.denominator);

		//
		return result;
	}

	/**
	 * Percentage string.
	 * 
	 * @return the string
	 */
	public String percentageString()
	{
		String result;

		result = percentageString(this.numerator, this.denominator);

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = this.numerator + "/" + this.denominator;

		//
		return result;
	}

	/**
	 * Percentage.
	 * 
	 * @param numerator
	 *            the numerator
	 * @param denominator
	 *            the denominator
	 * @return the long
	 * @throws Exception
	 *             the exception
	 */
	public static long percentage(final long numerator, final long denominator) throws Exception
	{
		long result;

		if (denominator == 0)
		{
			throw new Exception("denominator is zero");
		}
		else
		{
			result = Math.round(numerator * 100 / denominator);
		}

		//
		return result;
	}

	/**
	 * Percentage full string.
	 * 
	 * @param numerator
	 *            the numerator
	 * @param denominator
	 *            the denominator
	 * @return the string
	 */
	public static String percentageFullString(final long numerator, final long denominator)
	{
		String result;

		result = percentageString(numerator, denominator);

		//
		return result;
	}

	/**
	 * Percentage string.
	 * 
	 * @param numerator
	 *            the numerator
	 * @param denominator
	 *            the denominator
	 * @return the string
	 */
	public static String percentageString(final long numerator, final long denominator)
	{
		String result;

		try
		{
			long value = percentage(numerator, denominator);

			if (numerator == 0)
			{
				result = "0%";
			}
			else if (value == 0)
			{
				result = "~0%";
			}
			else if (value < 10)
			{
				result = "0" + value + "%";
			}
			else
			{
				result = value + "%";
			}
		}
		catch (Exception exception)
		{
			result = "--%";
		}

		//
		return result;
	}
}
