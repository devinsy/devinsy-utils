/*
 * Copyright (C) 2008-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

/**
 * This class defines a content file.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class DataFile
{
	public static int NOID = 0;
	public static int DEFAULT_SIZE = 0;

	//
	private int id;
	private int contentId;
	private String name;
	private long size;
	private byte[] data;
	private String creationDate;
	private String creationUser;

	/**
	 * Instantiates a new data file.
	 */
	public DataFile()
	{
		this.id = NOID;
		this.contentId = NOID;
		this.name = null;
		this.size = DEFAULT_SIZE;
		this.data = null;
		this.creationDate = null;
		this.creationUser = null;
	}

	/**
	 * Instantiates a new data file.
	 * 
	 * @param contentId
	 *            the content id
	 * @param name
	 *            the name
	 * @param size
	 *            the size
	 * @param data
	 *            the data
	 */
	public DataFile(final int contentId, final String name, final long size, final byte[] data)
	{
		this.id = NOID;
		this.contentId = contentId;
		this.name = name;
		this.size = size;
		this.data = data;
		this.creationDate = null;
		this.creationUser = null;
	}

	/**
	 * Instantiates a new data file.
	 * 
	 * @param name
	 *            the name
	 * @param size
	 *            the size
	 * @param data
	 *            the data
	 */
	public DataFile(final String name, final long size, final byte[] data)
	{
		this.id = NOID;
		this.contentId = NOID;
		this.name = name;
		this.size = size;
		this.data = data;
		this.creationDate = null;
		this.creationUser = null;
	}

	/**
	 * Content id.
	 * 
	 * @return the int
	 */
	public int contentId()
	{
		int result;

		result = this.contentId;

		//
		return result;
	}

	/**
	 * Creation date.
	 * 
	 * @return the string
	 */
	public String creationDate()
	{
		String result;

		result = this.creationDate;

		//
		return result;
	}

	/**
	 * Creation user.
	 * 
	 * @return the string
	 */
	public String creationUser()
	{
		String result;

		result = this.creationUser;

		//
		return result;
	}

	/**
	 * Data.
	 * 
	 * @return the byte[]
	 */
	public byte[] data()
	{
		byte[] result;

		result = this.data;

		//
		return result;
	}

	/**
	 * Id.
	 * 
	 * @return the int
	 */
	public int id()
	{
		int result;

		result = this.id;

		//
		return result;
	}

	/**
	 * Name.
	 * 
	 * @return the string
	 */
	public String name()
	{
		String result;

		result = this.name;

		//
		return result;
	}

	/**
	 * Sets the content id.
	 * 
	 * @param contentId
	 *            the new content id
	 */
	public void setContentId(final int contentId)
	{
		this.contentId = contentId;
	}

	/**
	 * Sets the creation date.
	 * 
	 * @param creationDate
	 *            the new creation date
	 */
	public void setCreationDate(final String creationDate)
	{
		if (creationDate == null)
		{
			this.creationDate = "";
		}
		else
		{
			this.creationDate = creationDate;
		}
	}

	/**
	 * Sets the creation user.
	 * 
	 * @param creationUser
	 *            the new creation user
	 */
	public void setCreationUser(final String creationUser)
	{
		if (creationUser == null)
		{
			this.creationUser = "";
		}
		else
		{
			this.creationUser = creationUser;
		}
	}

	/**
	 * Sets the data.
	 * 
	 * @param data
	 *            the new data
	 */
	public void setData(final byte[] data)
	{
		this.data = data;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(final int id)
	{
		this.id = id;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(final String name)
	{
		if (name == null)
		{
			this.name = "";
		}
		else
		{
			this.name = name;
		}
	}

	/**
	 * Sets the size.
	 * 
	 * @param size
	 *            the new size
	 */
	public void setSize(final long size)
	{
		if (size >= 0)
		{
			this.size = size;
		}
		else
		{
			this.size = 0;
		}
	}

	/**
	 * Size.
	 * 
	 * @return the long
	 */
	public long size()
	{
		long result;

		result = this.size;

		//
		return result;
	}
}
