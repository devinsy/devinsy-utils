/*
 * Copyright (C) 2010, 2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.io.File;
import java.util.Iterator;

/**
 * Used by FileIterator class.
 */
public class FileIteratorState implements Iterator<File>
{
	private File[] files;
	private int currentIndex;

	/**
	 * Instantiates a new file iterator state.
	 * 
	 * @param files
	 *            the files
	 */
	public FileIteratorState(final File[] files)
	{
		// Initialize the state.
		this.currentIndex = 0;

		if (files == null)
		{
			this.files = new File[0];
		}
		else
		{
			this.files = files;
		}
	}

	/**
	 * Useful for the depth zero, otherwise parent path is lost.
	 * 
	 * @param pathnames
	 *            the pathnames
	 */
	public FileIteratorState(final String[] pathnames)
	{
		// Initialize the state.
		this.currentIndex = 0;

		this.files = new File[pathnames.length];
		for (int pathnameIndex = 0; pathnameIndex < pathnames.length; pathnameIndex++)
		{
			this.files[pathnameIndex] = new File(pathnames[pathnameIndex]);
		}
	}

	/**
	 * Current file.
	 * 
	 * @return the file
	 */
	protected File currentFile()
	{
		File result;

		if (this.currentIndex >= this.files.length)
		{
			result = null;
		}
		else
		{
			result = this.files[this.currentIndex];
		}

		//
		return result;
	}

	/**
	 * Current index.
	 * 
	 * @return the int
	 */
	protected int currentIndex()
	{
		int result;

		result = this.currentIndex;

		//
		return result;
	}

	/**
	 * Files.
	 * 
	 * @return the file[]
	 */
	protected File[] files()
	{
		File[] result;

		result = this.files;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext()
	{
		boolean result;

		if (this.currentFile() == null)
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public File next()
	{
		File result;

		result = this.currentFile();
		this.currentIndex += 1;

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove()
	{
	}

	/**
	 * Reset.
	 */
	public void reset()
	{
		this.currentIndex = 0;
	}
}
