/*
 * Copyright (C) 2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.io.File;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Pattern;

/**
 * The Class FileIterator.
 */
public class FileIterator extends Vector<FileIteratorState> implements Iterator<File>
{
	private static final long serialVersionUID = 8790133455427427766L;

	private int currentDepth;
	private Pattern pattern;
	private File previous;
	private boolean followLinks;

	/**
	 * Instantiates a new file iterator.
	 * 
	 * @param root
	 *            the root
	 */
	public FileIterator(final File root)
	{
		super();

		String[] pathnames;
		if (root == null)
		{
			pathnames = null;
		}
		else
		{
			pathnames = new String[1];
			pathnames[0] = root.getPath();
		}

		init(pathnames, null, false);
	}

	/**
	 * Instantiates a new file iterator.
	 * 
	 * @param root
	 *            the root
	 * @param filter
	 *            the filter
	 * @param followLinks
	 *            the follow links
	 */
	public FileIterator(final File root, final String filter, final boolean followLinks)
	{
		super();

		String[] pathnames;
		if (root == null)
		{
			pathnames = null;
		}
		else
		{
			pathnames = new String[1];
			pathnames[0] = root.getPath();
		}

		init(pathnames, filter, followLinks);
	}

	/**
	 *
	 */
	public FileIterator(final String pathname, final String filter, final boolean followLinks)
	{
		super();

		String[] pathnames;
		if (pathname == null)
		{
			pathnames = null;
		}
		else
		{
			pathnames = new String[1];
			pathnames[0] = pathname;
		}

		init(pathnames, filter, followLinks);
	}

	/**
	 * Instantiates a new file iterator.
	 * 
	 * @param pathnames
	 *            the pathnames
	 * @param filter
	 *            the filter
	 * @param followLinks
	 *            the follow links
	 */
	public FileIterator(final String[] pathnames, final String filter, final boolean followLinks)
	{
		super();

		init(pathnames, filter, followLinks);
	}

	/**
	 * Current file.
	 * 
	 * @return the file
	 */
	public File currentFile()
	{
		File result;

		result = this.currentState().currentFile();

		//
		return result;
	}

	/**
	 * Current state.
	 * 
	 * @return the file iterator state
	 */
	protected FileIteratorState currentState()
	{
		FileIteratorState result;

		result = this.get(this.currentDepth);

		//
		return result;
	}

	/**
	 * Directory final countdown.
	 * 
	 * @return the int
	 */
	public int directoryFinalCountdown()
	{
		int result;

		result = 0;
		while (this.hasNext())
		{
			if (this.next().isDirectory())
			{
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public int fileFinalCountdown()
	{
		int result;

		result = 0;
		while (this.hasNext())
		{
			if (!this.next().isDirectory())
			{
				result += 1;
			}
		}

		//
		return result;
	}

	/**
	 * Filter.
	 * 
	 * @return the string
	 */
	protected String filter()
	{
		String result;

		if (this.pattern == null)
		{
			result = ".*";
		}
		else
		{
			result = this.pattern.toString();
		}

		//
		return result;
	}

	/**
	 * Final countdown.
	 * 
	 * @return the int
	 */
	public int finalCountdown()
	{
		int result;

		result = 0;
		while (this.next() != null)
		{
			result += 1;
		}

		//
		return result;
	}

	/**
	 * Follow.
	 * 
	 * @param file
	 *            the file
	 * @return true, if successful
	 */
	public boolean follow(final File file)
	{
		boolean result;

		result = false;
		try
		{
			if ((this.followLinks) || (!isLink(file)))
			{
				result = true;
			}
			else
			{
				result = false;
			}

			// System.out.println("FOLLOWWWWW=[" + file.getPath() + "][" +
			// this.followLinks + "][" + isLink(file) + "][" + result + "]");
		}
		catch (Exception exception)
		{
			System.err.println("ERROR with file [" + this.next() + "]: " + exception.getMessage());
			result = false;
		}

		//
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext()
	{
		boolean result;

		result = this.currentState().hasNext();

		//
		return result;
	}

	/**
	 * Inits the.
	 * 
	 * @param pathnames
	 *            the pathnames
	 * @param filter
	 *            the filter
	 * @param followLinks
	 *            the follow links
	 */
	protected void init(final String[] pathnames, final String filter, final boolean followLinks)
	{
		setFilter(filter);
		this.followLinks = followLinks;
		this.previous = null;

		this.currentDepth = 0;
		this.add(new FileIteratorState(pathnames));

		shift();
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#next()
	 */
	@Override
	public File next()
	{
		File result;

		result = this.currentState().next();
		this.previous = result;
		if (result != null)
		{
			if (result.isDirectory())
			{
				this.push(result);
			}
		}

		shift();

		//
		return result;
	}

	/**
	 * Pattern.
	 * 
	 * @return the pattern
	 */
	public Pattern pattern()
	{
		Pattern result;

		result = this.pattern;

		//
		return result;
	}

	/**
	 * Pop.
	 */
	public void pop()
	{
		this.removeElementAt(this.currentDepth);
		this.currentDepth -= 1;
	}

	/**
	 * Push.
	 * 
	 * @param file
	 *            the file
	 */
	public void push(final File file)
	{
		if ((file != null) && (file.isDirectory()))
		{
			this.add(new FileIteratorState(file.listFiles()));
			this.currentDepth += 1;
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove()
	{
		if (this.previous != null)
		{
			this.previous.delete();
			this.previous = null;
		}
	}

	/**
	 * Reset.
	 */
	public void reset()
	{
		this.currentDepth = 0;
		this.previous = null;
		if (this.size() > 0)
		{
			this.get(0).reset();
			FileIteratorState firstState = this.get(0);
			this.removeAllElements();
			this.add(firstState);
		}

		shift();
	}

	/**
	 * Sets the filter.
	 * 
	 * @param filter
	 *            the new filter
	 */
	protected void setFilter(final String filter)
	{
		if (filter == null)
		{
			this.pattern = null;
		}
		else
		{
			this.pattern = Pattern.compile(filter);
		}
	}

	/**
	 * Set indexes to the good next item.
	 */
	public void shift()
	{
		boolean ended = false;
		while (!ended)
		{
			File next = this.currentFile();

			if (next == null)
			{
				if (this.currentDepth == 0)
				{
					ended = true;
				}
				else
				{
					this.pop();
				}
			}
			else
			{
				if (((this.pattern == null) || (this.pattern.matcher(next.getPath()).matches())) && (follow(next)))
				{
					ended = true;
				}
				else
				{
					this.currentState().next();

					if (next.isDirectory() && (follow(next)))
					{
						this.push(next);
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Vector#toString()
	 */
	@Override
	public String toString()
	{
		String result;

		result = "[depth=" + this.currentDepth + "][index=" + this.get(this.currentDepth).currentIndex() + "/" + this.get(this.currentDepth).files().length + "]";

		//
		return result;
	}

	/**
	 * Checks if is link.
	 * 
	 * @param file
	 *            the file
	 * @return true, if is link
	 * @throws Exception
	 *             the exception
	 */
	public static boolean isLink(final File file) throws Exception
	{
		boolean result;

		if ((file.exists()) && (file.getCanonicalPath().equals(file.getAbsolutePath())))
		{
			result = false;
		}
		else
		{
			result = true;
		}

		//
		return result;
	}
}
