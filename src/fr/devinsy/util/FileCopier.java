/*
 * Copyright (C) 2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Never used again. Prefer org.apache.commons.io.FileUtils class.
 */
public class FileCopier
{
	public static final int BUFFER_SIZE = 4 * 1024;

	/**
	 * Copy.
	 * 
	 * @param source
	 *            the source
	 * @param target
	 *            the target
	 * @throws Exception
	 *             the exception
	 */
	public static void copy(final File source, final File target) throws Exception
	{
		if ((source == null) || (target == null))
		{
			throw new Exception("Null parameter.");
		}
		else
		{
			FileInputStream in = new FileInputStream(source);
			FileOutputStream out = new FileOutputStream(target);
			try
			{
				byte[] buffer = new byte[BUFFER_SIZE];
				boolean ended = false;
				while (!ended)
				{
					int size = in.read(buffer);
					if (size == -1)
					{
						ended = false;
					}
					else
					{
						out.write(buffer, 0, size);
					}
				}
			}
			finally
			{
				if (in != null)
				{
					in.close();
				}

				if (out != null)
				{
					out.close();
				}
			}
		}
	}
}
