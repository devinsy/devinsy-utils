/*
 * Copyright (C) 2008-2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import fr.devinsy.util.strings.StringList;

/**
 * The Class ToolBox.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class ToolBox
{
	/**
	 * Clean.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String clean(final String source)
	{
		String result;

		result = source.replaceAll("[^\\w ]", " ");

		//
		return result;
	}

	/**
	 * Returns information about the calling class of a calledClass.
	 * 
	 * @param calledClassName
	 *            the class name which is the subject of the search.
	 * 
	 * @return information about the calling class.
	 */
	public static StackTraceElement getCaller(final String calledClassName)
	{
		StackTraceElement result;

		//
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		// System.out.println("////////////////////////////");
		// for (int i = 0; (i < stack.length) && (i < 4); i++) {
		// System.out.println(i + " " + stack[i].getClassName());
		// }

		// Search for first entry of class called.
		boolean ended = false;
		Integer indexOfCalled = null;
		int depth = 1;
		while (!ended)
		{
			if (depth < stack.length)
			{
				String currentClassName = stack[depth].getClassName();
				if (currentClassName.equals(calledClassName))
				{
					ended = true;
					indexOfCalled = Integer.valueOf(depth);
				}
				else
				{
					depth += 1;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		// Search for caller of class called.
		if (indexOfCalled == null)
		{
			result = null;
		}
		else
		{
			result = null;
			ended = false;
			depth = indexOfCalled;
			while (!ended)
			{
				if (depth < stack.length)
				{
					String currentClassName = stack[depth].getClassName();
					if (currentClassName.equals(calledClassName))
					{
						depth += 1;
					}
					else
					{
						ended = true;
						result = stack[depth];
					}
				}
				else
				{
					ended = true;
					result = null;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Index of.
	 * 
	 * @param pattern
	 *            the pattern
	 * @param source
	 *            the source
	 * @return the int
	 */
	public static int indexOf(final String pattern, final List<String> source)
	{
		int result;

		if (source == null)
		{
			result = -1;
		}
		else
		{
			boolean ended = false;
			result = -1;
			int currentIndex = 0;
			while (!ended)
			{
				if (currentIndex < source.size())
				{
					String sourceString = source.get(currentIndex);
					if (StringUtils.equals(sourceString, pattern))
					{
						ended = true;
						result = currentIndex;
					}
					else
					{
						currentIndex += 1;
					}
				}
				else
				{
					ended = true;
					currentIndex = -1;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Matches any.
	 * 
	 * @param string
	 *            the string
	 * @param targets
	 *            the targets
	 * @return true, if successful
	 */
	public static boolean matchesAny(final String string, final String... targets)
	{
		boolean result;

		if ((string == null) || (targets == null))
		{
			result = false;
		}
		else
		{
			//
			boolean ended = false;
			int index = 0;
			result = false;
			while (!ended)
			{
				if (index < targets.length)
				{
					if (StringUtils.equals(string, targets[index]))
					{
						ended = true;
						result = true;
					}
					else
					{
						index += 1;
					}
				}
				else
				{
					ended = true;
					result = false;
				}
			}
		}

		//
		return result;
	}

	/**
	 * Sort.
	 * 
	 * @param source
	 *            the source
	 * @return the double[]
	 */
	public static Double[] sort(final Set<Double> source)
	{
		Double[] result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = new Double[source.size()];

			source.toArray(result);
			Arrays.sort(result);
		}

		//
		return result;
	}

	/**
	 * Concatenates int values from an array, adding decoration strings.
	 * 
	 * @param values
	 *            Source of int values.
	 * @param prefix
	 *            Decoration to put on start.
	 * @param separator
	 *            Decoration to put between values.
	 * @param postfix
	 *            Decoration to put on end.
	 * 
	 * @return A decorated string representing the int values.
	 */
	public static String toString(final int[] values, final String prefix, final String separator, final String postfix)
	{
		String result;

		StringList buffer = new StringList();

		//
		if (prefix != null)
		{
			buffer.append(prefix);
		}

		//
		boolean firstPassed = false;
		for (int value : values)
		{
			if (firstPassed)
			{
				buffer.append(separator);
			}
			else
			{
				firstPassed = true;
			}
			buffer.append(value);
		}

		//
		if (postfix != null)
		{
			buffer.append(postfix);
		}

		//
		result = buffer.toString();

		//
		return result;
	}

	/**
	 * To string.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 */
	public static String toString(final String source)
	{
		String result;

		if (source == null)
		{
			result = "";
		}
		else
		{
			result = source;
		}

		//
		return result;
	}
}
