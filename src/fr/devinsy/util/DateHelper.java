/*
 * Copyright (C) 2010,2013-2014,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class groups function to help in Calendar manipulation.
 * 
 * SimpleDateFormat is not used cause does not thread safe?
 */
public class DateHelper
{
	// static private final Logger logger =
	// LoggerFactory.getLogger(DateHelper.class);

	private static final String EUROPEAN_DATE_FORMAT = "%02d/%02d/%04d";
	private static final String SHORT_EUROPEAN_DATE_FORMAT = "%02d/%02d";
	private static final String RAW_DATE_FORMAT = "%04d%02d%02d";
	private static final String ISO_DATE_FORMAT = "%04d-%02d-%02d";
	private static final String AMERICAN_DATE_FORMAT = "%02d/%02d/%04d";

	private static final String EUROPEAN_DATE_PATTERN = "^([0123]{0,1}\\d)/([01]{0,1}\\d)/(\\d\\d\\d\\d)$";
	private static final String RAW_DATE_PATTERN = "^(\\d\\d\\d\\d)([01]\\d)([0123]\\d)$";
	private static final String ISO_DATE_PATTERN = "^(\\d\\d\\d\\d)-([01]\\d)-([0123]\\d)$";
	private static final String AMERICAN_DATE_PATTERN = "^([01]{0,1}\\d)/([0123]{0,1}\\d)/(\\d\\d\\d\\d)$";

	/**
	 * American format.
	 * 
	 * @param time
	 *            the time
	 * @return the string
	 */
	public static String americanFormat(final Calendar time)
	{
		String result;

		if (time == null)
		{
			result = "";
		}
		else
		{
			result = String.format(AMERICAN_DATE_FORMAT, time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH), time.get(Calendar.DAY_OF_MONTH));
		}

		//
		return result;
	}

	/**
	 * European format.
	 * 
	 * @param time
	 *            the time
	 * @return the string
	 */
	public static String europeanFormat(final Calendar time)
	{
		String result;

		if (time == null)
		{
			result = "";
		}
		else
		{
			result = String.format(EUROPEAN_DATE_FORMAT, time.get(Calendar.DAY_OF_MONTH), time.get(Calendar.MONTH) + 1, time.get(Calendar.YEAR));
		}

		//
		return result;
	}

	/**
	 * Checks if is american format.
	 * 
	 * @param date
	 *            the date
	 * @return true, if is american format
	 */
	public static boolean isAmericanFormat(final String date)
	{
		boolean result;

		if (date == null)
		{
			result = false;
		}
		else
		{
			result = date.matches(AMERICAN_DATE_PATTERN);
		}

		//
		return result;
	}

	/**
	 * Checks if is european format.
	 * 
	 * @param date
	 *            the date
	 * @return true, if is european format
	 */
	public static boolean isEuropeanFormat(final String date)
	{
		boolean result;

		if (date == null)
		{
			result = false;
		}
		else
		{
			result = date.matches(EUROPEAN_DATE_PATTERN);
		}

		//
		return result;
	}

	/**
	 * Checks if is ISO format.
	 * 
	 * @param date
	 *            the date
	 * @return true, if is ISO format
	 */
	public static boolean isISOFormat(final String date)
	{
		boolean result;

		if (date == null)
		{
			result = false;
		}
		else
		{
			result = date.matches(ISO_DATE_PATTERN);
		}

		//
		return result;
	}

	/**
	 * ISO format.
	 * 
	 * @param time
	 *            the time
	 * @return the string
	 */
	public static String ISOFormat(final Calendar time)
	{
		String result;

		if (time == null)
		{
			result = "";
		}
		else
		{
			result = String.format(ISO_DATE_FORMAT, time.get(Calendar.YEAR), time.get(Calendar.MONTH) + 1, time.get(Calendar.DAY_OF_MONTH));
		}

		//
		return result;
	}

	/**
	 * Checks if is raw format.
	 * 
	 * @param date
	 *            the date
	 * @return true, if is raw format
	 */
	public static boolean isRawFormat(final String date)
	{
		boolean result;

		if (date == null)
		{
			result = false;
		}
		else
		{
			result = date.matches(RAW_DATE_PATTERN);
		}

		//
		return result;
	}

	/**
	 * Checks if is valid date.
	 * 
	 * @param date
	 *            the date
	 * @return true, if is valid date
	 */
	public static boolean isValidDate(final String date)
	{
		boolean result;

		if ((isEuropeanFormat(date)) || (isRawFormat(date)) || (isISOFormat(date)) || (isAmericanFormat(date)))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * Parses the american date.
	 * 
	 * @param date
	 *            the date
	 * @return the calendar
	 */
	public static Calendar parseAmericanDate(final String date)
	{
		Calendar result;

		Pattern pattern = Pattern.compile(AMERICAN_DATE_PATTERN);
		Matcher matcher = pattern.matcher(date);

		if ((matcher.find()) && (matcher.groupCount() == 3))
		{
			result = new GregorianCalendar(Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(1)) - 1, Integer.parseInt(matcher.group(2)));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Note: European parsing test made before the American parsing one.
	 * 
	 * @param date
	 *            the date
	 * @return the calendar
	 */
	public static Calendar parseDate(final String date)
	{
		Calendar result;

		if (isEuropeanFormat(date))
		{
			result = parseEuropeanDate(date);
		}
		else if (isRawFormat(date))
		{
			result = parseRawDate(date);
		}
		else if (isISOFormat(date))
		{
			result = parseISODate(date);
		}
		else if (isAmericanFormat(date))
		{
			result = parseAmericanDate(date);
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Parses the european date.
	 * 
	 * @param date
	 *            the date
	 * @return the calendar
	 */
	public static Calendar parseEuropeanDate(final String date)
	{
		Calendar result;

		Pattern pattern = Pattern.compile(EUROPEAN_DATE_PATTERN);
		Matcher matcher = pattern.matcher(date);

		if ((matcher.find()) && (matcher.groupCount() == 3))
		{
			result = new GregorianCalendar(Integer.parseInt(matcher.group(3)), Integer.parseInt(matcher.group(2)) - 1, Integer.parseInt(matcher.group(1)));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Parses the ISO date.
	 * 
	 * @param date
	 *            the date
	 * @return the calendar
	 */
	public static Calendar parseISODate(final String date)
	{
		Calendar result;

		Pattern pattern = Pattern.compile(ISO_DATE_PATTERN);
		Matcher matcher = pattern.matcher(date);

		if ((matcher.find()) && (matcher.groupCount() == 3))
		{
			result = new GregorianCalendar(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)) - 1, Integer.parseInt(matcher.group(3)));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Parses the raw date.
	 * 
	 * @param date
	 *            the date
	 * @return the calendar
	 */
	public static Calendar parseRawDate(final String date)
	{
		Calendar result;

		Pattern pattern = Pattern.compile(RAW_DATE_PATTERN);
		Matcher matcher = pattern.matcher(date);

		if ((matcher.find()) && (matcher.groupCount() == 3))
		{
			result = new GregorianCalendar(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)) - 1, Integer.parseInt(matcher.group(3)));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * Raw format.
	 * 
	 * @param time
	 *            the time
	 * @return the string
	 */
	public static String rawFormat(final Calendar time)
	{
		String result;

		if (time == null)
		{
			result = "";
		}
		else
		{
			result = String.format(RAW_DATE_FORMAT, time.get(Calendar.YEAR), time.get(Calendar.MONTH), time.get(Calendar.DAY_OF_MONTH) + 1);
		}

		//
		return result;
	}

	/**
	 * Short european format.
	 * 
	 * @param time
	 *            the time
	 * @return the string
	 */
	public static String shortEuropeanFormat(final Calendar time)
	{
		String result;

		if (time == null)
		{
			result = "";
		}
		else
		{
			result = String.format(SHORT_EUROPEAN_DATE_FORMAT, time.get(Calendar.DAY_OF_MONTH), time.get(Calendar.MONTH) + 1);
		}

		//
		return result;
	}
}
