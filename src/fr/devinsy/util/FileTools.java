/*
 * Copyright (C) 2008-2015,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import fr.devinsy.util.strings.StringList;
import fr.devinsy.util.strings.StringListUtils;

/**
 * The Class FileTools.
 * 
 * @author Christian Pierre MOMON (christian.momon@devinsy.fr)
 */
public class FileTools
{
	public static final String DEFAULT_CHARSET_NAME = "UTF-8";

	/**
	 * Adds the before extension.
	 * 
	 * @param fileName
	 *            Source.
	 * @param addition
	 *            the addition
	 * @return Extension value or null.
	 */
	public static String addBeforeExtension(final String fileName, final String addition)
	{
		String result;

		if (fileName == null)
		{
			result = null;
		}
		else if (addition == null)
		{
			result = fileName;
		}
		else
		{
			//
			int separatorIndex = fileName.lastIndexOf('.');

			//
			if (separatorIndex > 0)
			{
				result = fileName.substring(0, separatorIndex) + addition + fileName.substring(separatorIndex);
			}
			else
			{
				result = fileName + addition;
			}
		}

		//
		return result;
	}

	/**
	 * Adds the to name.
	 * 
	 * @param file
	 *            Source.
	 * @param addition
	 *            the addition
	 * @return Extension value or null.
	 */
	public static File addToName(final File file, final String addition)
	{
		File result;

		if (file == null)
		{
			result = null;
		}
		else if (addition == null)
		{
			result = file;
		}
		else
		{
			//
			String sourceFileName = file.getAbsolutePath();
			String targetFileName = addBeforeExtension(sourceFileName, addition);
			result = new File(targetFileName);
		}

		//
		return result;
	}

	/**
	 * Get the extension of a file.
	 * 
	 * @param file
	 *            Source.
	 * 
	 * @return Extension value or null.
	 */
	public static String getExtension(final File file)
	{
		String result;

		if (file == null)
		{
			result = null;
		}
		else
		{
			result = getExtension(file.getName());
		}

		//
		return result;
	}

	/**
	 * Get the extension of a file.
	 * 
	 * <ul>
	 * <li>getExtension(null) = null</li>
	 * <li>getExtension("") = null</li>
	 * <li>getExtension("abc") = null</li>
	 * <li>getExtension("abc.efg") = "efg"</li>
	 * </ul>
	 * 
	 * @param fileName
	 *            the file name
	 * @return Extension value or null.
	 * @deprecated See
	 *             <code>org.apache.commons.io.FilenameUtils.getExtension</code>
	 */
	@Deprecated
	public static String getExtension(final String fileName)
	{
		String result;

		if (fileName == null)
		{
			result = null;
		}
		else
		{
			int separatorIndex = fileName.lastIndexOf('.');
			if (separatorIndex > 0)
			{
				result = fileName.substring(separatorIndex + 1).toLowerCase();
			}
			else
			{
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * Load.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String load(final File source) throws IOException
	{
		String result;

		result = load(source, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Load.
	 * 
	 * @param source
	 *            the source
	 * @param charsetName
	 *            the charset name
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String load(final File source, final String charsetName) throws IOException
	{
		String result;

		result = loadToStringBuffer(source, charsetName).toString();

		//
		return result;
	}

	/**
	 * Load.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String load(final URL source) throws IOException
	{
		String result;

		result = load(source, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Load.
	 * 
	 * @param source
	 *            the source
	 * @param charsetName
	 *            the charset name
	 * @return the string
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static String load(final URL source, final String charsetName) throws IOException
	{
		String result;

		//
		StringBuffer buffer = new StringBuffer(source.openConnection().getContentLength() + 1);
		read(buffer, source.openStream(), charsetName);

		//
		result = buffer.toString();

		//
		return result;
	}

	/**
	 * Load string list.
	 * 
	 * @param source
	 *            the source
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList loadStringList(final File source) throws IOException
	{
		StringList result;

		result = loadStringList(source, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Load string list.
	 * 
	 * @param file
	 *            the file
	 * @param charsetName
	 *            the charset name
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList loadStringList(final File file, final String charsetName) throws IOException
	{
		StringList result;

		result = StringListUtils.load(file, charsetName);

		//
		return result;
	}

	/**
	 * Load to string buffer.
	 * 
	 * @param source
	 *            the source
	 * @return the string buffer
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringBuffer loadToStringBuffer(final File source) throws IOException
	{
		StringBuffer result;

		result = loadToStringBuffer(source, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Load to string buffer.
	 * 
	 * @param file
	 *            the file
	 * @param charsetName
	 *            the charset name
	 * @return the string buffer
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringBuffer loadToStringBuffer(final File file, final String charsetName) throws IOException
	{
		StringBuffer result;

		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));

			boolean ended = false;
			final String LINE_SEPARATOR = System.getProperty("line.separator");
			result = new StringBuffer((int) file.length() + 1);
			while (!ended)
			{
				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					result.append(line).append(LINE_SEPARATOR);
				}
			}
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException exception)
			{
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Load to string list.
	 * 
	 * @param source
	 *            the source
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList loadToStringList(final File source) throws IOException
	{
		StringList result;

		result = loadToStringList(source, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Load to string list.
	 * 
	 * @param file
	 *            the file
	 * @param charsetName
	 *            the charset name
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList loadToStringList(final File file, final String charsetName) throws IOException
	{
		StringList result;

		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(new FileInputStream(file), charsetName));

			boolean ended = false;
			final String LINE_SEPARATOR = System.getProperty("line.separator");
			result = new StringList();
			while (!ended)
			{
				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					result.append(line).append(LINE_SEPARATOR);
				}
			}
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException exception)
			{
				exception.printStackTrace();
			}
		}

		//
		return result;
	}

	/**
	 * Load to string list.
	 * 
	 * @param source
	 *            the source
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList loadToStringList(final URL source) throws IOException
	{
		StringList result;

		result = loadToStringList(source, DEFAULT_CHARSET_NAME);

		//
		return result;
	}

	/**
	 * Load to string list.
	 * 
	 * @param source
	 *            the source
	 * @param charsetName
	 *            the charset name
	 * @return the string list
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static StringList loadToStringList(final URL source, final String charsetName) throws IOException
	{
		StringList result;

		//
		result = StringListUtils.load(source, charsetName);

		//
		return result;
	}

	/**
	 * Read.
	 * 
	 * @param out
	 *            the out
	 * @param is
	 *            the is
	 * @param charsetName
	 *            the charset name
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void read(final StringBuffer out, final InputStream is, final String charsetName) throws IOException
	{
		BufferedReader in = null;
		try
		{
			in = new BufferedReader(new InputStreamReader(is, charsetName));

			boolean ended = false;
			final String LINE_SEPARATOR = System.getProperty("line.separator");

			while (!ended)
			{
				String line = in.readLine();

				if (line == null)
				{
					ended = true;
				}
				else
				{
					out.append(line).append(LINE_SEPARATOR);
				}
			}
		}
		finally
		{
			try
			{
				if (in != null)
				{
					in.close();
				}
			}
			catch (IOException exception)
			{
				exception.printStackTrace();
			}
		}
	}

	/**
	 * Removes the extension.
	 * 
	 * @param source
	 *            the source
	 * @return the string
	 * @deprecated See
	 *             <code>org.apache.commons.io.FilenameUtils.removeExtension</code>
	 */
	@Deprecated
	public static String removeExtension(final String source)
	{
		String result;

		if (source == null)
		{
			result = source;
		}
		else
		{
			int separatorIndex = source.lastIndexOf('.');

			//
			if (separatorIndex > 0)
			{
				result = source.substring(0, separatorIndex);
			}
			else
			{
				result = source;
			}
		}

		//
		return result;
	}

	/**
	 * Save.
	 * 
	 * @param file
	 *            the file
	 * @param source
	 *            the source
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public static void save(final File file, final String source) throws UnsupportedEncodingException, FileNotFoundException
	{
		PrintWriter out = null;
		try
		{
			out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file), DEFAULT_CHARSET_NAME));

			out.println(source);
		}
		finally
		{
			if (out != null)
			{
				out.close();
			}
		}
	}

	/**
	 * Save.
	 * 
	 * @param file
	 *            the file
	 * @param source
	 *            the source
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public static void save(final File file, final StringBuffer source) throws UnsupportedEncodingException, FileNotFoundException
	{
		save(file, source.toString());
	}

	/**
	 * Save.
	 * 
	 * @param file
	 *            the file
	 * @param source
	 *            the source
	 * @throws UnsupportedEncodingException
	 *             the unsupported encoding exception
	 * @throws FileNotFoundException
	 *             the file not found exception
	 */
	public static void save(final File file, final StringList source) throws UnsupportedEncodingException, FileNotFoundException
	{
		StringListUtils.save(file, source);
	}

	/**
	 * Sets the extension.
	 * 
	 * @param source
	 *            the source
	 * @param extension
	 *            the extension
	 * @return the file
	 */
	public static File setExtension(final File source, final String extension)
	{
		File result;

		if ((source == null) || (extension == null))
		{
			result = source;
		}
		else
		{
			result = new File(setExtension(source.getAbsolutePath(), extension));
		}

		//
		return result;
	}

	/**
	 * Sets the extension.
	 * 
	 * @param source
	 *            the source
	 * @param extension
	 *            the extension
	 * @return the string
	 */
	public static String setExtension(final String source, final String extension)
	{
		String result;

		if ((source == null) || (extension == null))
		{
			result = source;
		}
		else
		{
			int separatorIndex = source.lastIndexOf('.');

			//
			if (separatorIndex > 0)
			{
				String prefix = source.substring(0, separatorIndex);
				if (prefix.endsWith(extension))
				{
					result = prefix;
				}
				else
				{
					result = prefix + extension;
				}
			}
			else
			{
				result = source + extension;
			}
		}

		//
		return result;
	}
}
