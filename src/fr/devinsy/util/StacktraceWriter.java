/*
 * Copyright (C) 2010,2013-2016,2017 Christian Pierre MOMON
 * 
 * This file is part of Devinsy-utils.
 * 
 * Devinsy-utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Devinsy-utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Devinsy-utils.  If not, see <http://www.gnu.org/licenses/>
 */
package fr.devinsy.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * The Class StacktraceWriter.
 * 
 * @deprecated use <code>SLF4J.Logger.error("blabla", exception)</code> method
 *             or the ExceptionUtils.getStackTrace(throwable).
 */
@Deprecated
public class StacktraceWriter
{

	/**
	 * To string.
	 * 
	 * @param exception
	 *            the exception
	 * @return the string
	 * @deprecated use <code>SLF4J.Logger.error("blabla", exception)</code>
	 *             method or the ExceptionUtils.getStackTrace(throwable).
	 */
	@Deprecated
	public static String toString(final Exception exception)
	{
		String result;

		ByteArrayOutputStream out = new ByteArrayOutputStream(50000);
		exception.printStackTrace(new PrintStream(out));
		result = out.toString();

		//
		return result;
	}
}
